﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.DataObjects;

namespace Otus.Teaching.Concurrency.Import.Loader.Configuration
{
    public class ConfigGenerator
    {
        private readonly LoaderConfig _loaderConfig;
        private const int _defaultDataCount = 1000;
        private const int _defaultNumberOfAttempts = 10;
        private readonly string _defaultFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        private readonly RepositoryType _defaultRepositoryType = RepositoryType.MssqlCustomerRepository;

        public LoaderConfig GetConfig() => _loaderConfig;

        public ConfigGenerator()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true);
            var config = configBuilder.Build();

            var dataCountVal = config["DataGenerationCount"];
            var numberOfAttempts = config["NumberOfAttempts"];
            var repositoryType = config["RepositoryType"];

            _loaderConfig = new LoaderConfig
            {
                ExternalDataGeneration = bool.TryParse(config["DataGenerationByExternalApplication"], out var res) && res,
                ExternalApplicationName = config["ExternalApplicationName"],
                ExternalApplicationPath = config["ExternalApplicationPath"],
                OutputFileName = config["DataFilePath"]
            };

            if (string.IsNullOrEmpty(_loaderConfig.OutputFileName))
            {
                _loaderConfig.OutputFileName = _defaultFilePath;
            }

            _loaderConfig.GenerateDataCount = string.IsNullOrEmpty(dataCountVal)
                ? _defaultDataCount
                : int.TryParse(dataCountVal, out var configDataCount)
                    ? configDataCount
                    : _defaultDataCount;

            _loaderConfig.NumberOfAttempts = string.IsNullOrEmpty(numberOfAttempts)
                ? _defaultDataCount
                : int.TryParse(numberOfAttempts, out var configNumberOfAttempts)
                    ? configNumberOfAttempts
                    : _defaultNumberOfAttempts;

            _loaderConfig.RepositoryType = string.IsNullOrEmpty(repositoryType)
                ? _defaultRepositoryType
                : repositoryType == "MssqlCustomerRepository"
                    ? RepositoryType.MssqlCustomerRepository
                    : RepositoryType.SqlLiteCustomerRepository;

            if (_loaderConfig.GenerateDataCount <= 0)
            {
                throw new ArgumentException("The number of DataGenerationCount cannot be less than or equal to 0.");
            }

            PrintConfigData();
        }

        private void PrintConfigData()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Configuration");
            foreach (var item in _loaderConfig.GetType().GetProperties())
            {
                Console.WriteLine($"{item.Name}: {item.GetValue(_loaderConfig)}");
            }
            Console.ResetColor();
        }
    }
}
