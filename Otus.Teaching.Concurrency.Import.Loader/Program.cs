﻿using System;
using System.Diagnostics;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Utils;
using Otus.Teaching.Concurrency.Import.Core.DataObjects;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Loader.Configuration;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    //1. Для создания базы данных MS SQL можно использовать команду "update-database -context MsSqlContext", миграция создана.
    //2. База данных SQLite в виде файла ConcurrencyImport.db уже создана, и должна автоматически копироваться в выходной каталог программы.
    //3. Настройка поведения программы через appsettings.json.
    //4. Для повторного добавления данных в MS SQL придется очистить ее от предыдущих данных (TRUNCATE TABLE Customers), автоматическое удаление не предусмотрено.
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var loaderConfig = new ConfigGenerator().GetConfig();
            if (loaderConfig.ExternalDataGeneration)
            {
                var extLoader = new ExternalApplicationLoader(loaderConfig);
                if (extLoader.ProcessResult)
                {
                    LoadData(loaderConfig);
                }
            }
            else
            {
                GenerateCustomersDataFile(loaderConfig);
                LoadData(loaderConfig);
            }

            Console.ReadKey();
        }

        private static async void LoadData(LoaderConfig config)
        {
            var fileLoader = new XmlParser(config.OutputFileName);
            var xmlData = await fileLoader.ParseAsync();


            var loader = new DataLoader(xmlData, config);
            //loader.LoadData();
            loader.LoadDataAsync();

            //var loader = new FakeDataLoader();
            //loader.LoadData();
        }

        static void GenerateCustomersDataFile(LoaderConfig config)
        {
            if (System.IO.File.Exists(config.OutputFileName))
            {
                Console.WriteLine(
                    $"{Environment.NewLine}A file named \"{config.OutputFileName}\" already exists.{Environment.NewLine}Create a new file? [N - no]");
                var a = Console.ReadLine();
                if (a.ToUpper() == "N")
                {
                    return;
                }
            }
            var xmlGenerator = new XmlGenerator(config.OutputFileName, config.GenerateDataCount);
            xmlGenerator.Generate();
        }
    }
}