﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.DataObjects;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader : IDataLoader
    {
        public event EventHandler SuccessEndWork;
        public event EventHandler<Exception> WorkInterrupted;
        public event EventHandler<ThreadObject> ThreadHasProblem;
        public event EventHandler<ThreadObject> ThreadSuccessEndWork;

        public event EventHandler<LoaderObject> TaskHasProblem;
        public event EventHandler<LoaderObject> TaskSuccessEndWork;

        private readonly List<Customer> _customers;
        private readonly Customer[][] _curCollection;

        private int _countOfThreadEnded;
        private readonly LoaderConfig _config;
        private readonly Stopwatch _stopwatch;

        public DataLoader(IEnumerable<Customer> customers, LoaderConfig config)
        {
            _config = config;
            _stopwatch = new Stopwatch();
            _customers = customers.OrderBy(o => o.Id).ToList();

            var threadsCount = Environment.ProcessorCount;
            var collectionEndLen = _customers.Count % threadsCount;

            _curCollection = DefineCustomerCollection(_customers.Count, threadsCount, collectionEndLen);
            FillDataToCollection(threadsCount, collectionEndLen);
        }

        /// <summary> Наполняет данными _curCollection. </summary>
        private void FillDataToCollection(int threadsCount, int collectionEndLen)
        {
            if (_curCollection.Length == 1)
            {
                _curCollection[0] = _customers.ToArray();
                return;
            }

            var currentSkip = 0;
            var collectionSizeStep = _customers.Count / threadsCount;

            for (var j = 0; j < threadsCount; j++)
            {
                _curCollection[j] = _customers.Skip(currentSkip).Take(collectionSizeStep).ToArray();
                currentSkip += collectionSizeStep;
            }

            if (collectionEndLen != 0)
            {
                _curCollection[threadsCount] = _customers.Skip(currentSkip).Take(collectionEndLen).ToArray();
            }
        }

        /// <summary> Формирование структуры массива данных от количества процессоров и размера коллекции. </summary>
        private Customer[][] DefineCustomerCollection(int collectionCount, int threadsCount, int collectionEndLen)
        {
            return collectionCount < 100
                ? new Customer[1][] {_customers.ToArray()}
                : collectionEndLen == 0
                    ? new Customer[threadsCount][]
                    : new Customer[threadsCount + 1][];
        }

        public void LoadData()
        {
            Console.WriteLine("Save data to database...");

            _stopwatch.Start();

            this.SuccessEndWork += DataLoader_SuccessEndWork;
            this.WorkInterrupted += DataLoader_WorkInterrupted;
            this.ThreadHasProblem += DataLoader_ThreadHasProblem;
            this.ThreadSuccessEndWork += DataLoader_ThreadSuccessEndWork;

            for (var i = 0; i < _curCollection.Length; i++)
            {
                var thread = new Thread(Write);
                var threadObject = new ThreadObject { PartNumber = i, ThreadId = thread.ManagedThreadId, AllThreadCount = _curCollection.Length, CurThread = thread };
                thread.Start(threadObject);
            }
        }
        public void LoadDataAsync()
        {
            Console.WriteLine("Save data to database...");

            _stopwatch.Start();

            this.SuccessEndWork += DataLoader_SuccessEndWork;
            this.TaskHasProblem += DataLoader_TaskHasProblem;
            this.WorkInterrupted += DataLoader_WorkInterrupted;
            this.TaskSuccessEndWork += DataLoader_TaskSuccessEndWork;

            for (var i = 0; i < _curCollection.Length; i++)
            {
                var loaderObject = new LoaderObject { PartNumber = i, AllTaskCount = _curCollection.Length };
                var t = new Task(() => WriteAsync(loaderObject));
                loaderObject.Task = t;
                t.Start();
            }
        }

        private void DataLoader_ThreadHasProblem(object sender, ThreadObject e)
        {
            e.ErrorsCount++;
            Console.WriteLine($">> The thread has a problem: {e}.\n>> {e.LastException.Message}");
            if (e.ErrorsCount > _config.NumberOfAttempts)
            {
                WorkInterrupted?.Invoke(this, e.LastException);
                return;
            }

            Write(e);
        }
        private void DataLoader_TaskHasProblem(object sender, LoaderObject e)
        {
            e.ErrorsCount++;
            Console.WriteLine($">> The task has a problem: {e}.\n>> {e.LastException.Message}");
            if (e.ErrorsCount > _config.NumberOfAttempts)
            {
                WorkInterrupted?.Invoke(this, e.LastException);
                return;
            }

            WriteAsync(e);
        }

        private void DataLoader_ThreadSuccessEndWork(object sender, ThreadObject e)
        {
            Console.WriteLine($"Thread number {e.ThreadId} completed successfully. Elapsed time: {_stopwatch.Elapsed}.");
            if (e.AllThreadCount == ++_countOfThreadEnded)
            {
                SuccessEndWork?.Invoke(this, null);
            }
        }

        private void DataLoader_TaskSuccessEndWork(object sender, LoaderObject e)
        {
            Console.WriteLine($"Task id {e.Task.Id} completed successfully. Elapsed time: {_stopwatch.Elapsed}.");
            if (e.AllTaskCount == ++_countOfThreadEnded)
            {
                SuccessEndWork?.Invoke(this, null);
            }
            e.Task.GetAwaiter().GetResult();
        }

        private void DataLoader_SuccessEndWork(object sender, EventArgs e)
        {
            _stopwatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Data saving is complete. Elapsed time {_stopwatch.Elapsed}.");
            Console.ResetColor();
        }

        private void DataLoader_WorkInterrupted(object sender, Exception e)
        {
            Console.WriteLine(e.Message);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Data saving is interrupted.");
            Console.ResetColor();
        }

        private void Write(object tObject)
        {
            var threadObject = (ThreadObject)tObject;
            try
            {
                if (_config.RepositoryType == RepositoryType.MssqlCustomerRepository)
                {
                    using (var rep = new MssqlCustomerRepository())
                    {
                        rep.AddCustomers(_curCollection[threadObject.PartNumber]);
                    }
                }
                else
                {
                    using (var rep = new SqlLiteCustomerRepository())
                    {
                        rep.AddCustomers(_curCollection[threadObject.PartNumber]);
                    }
                }
            }
            catch (Exception e)
            {
                threadObject.LastException = e;
                ThreadHasProblem?.Invoke(this, threadObject);
                threadObject.CurThread.Join();
                return;
            }

            ThreadSuccessEndWork?.Invoke(this, threadObject);

            threadObject.CurThread.Join();
        }
        private async void WriteAsync(LoaderObject tObject)
        {
            try
            {
                if (_config.RepositoryType == RepositoryType.MssqlCustomerRepository)
                {
                    using (var rep = new MssqlCustomerRepository())
                    {
                        await rep.AddCustomersAsync(_curCollection[tObject.PartNumber]);
                    }
                }
                else
                {
                    using (var rep = new SqlLiteCustomerRepository())
                    {
                        await rep.AddCustomersAsync(_curCollection[tObject.PartNumber]);
                    }
                }
            }
            catch (Exception e)
            {
                tObject.LastException = e;
                TaskHasProblem?.Invoke(this, tObject);
                return;
            }

            TaskSuccessEndWork?.Invoke(this, tObject);
        }
    }
}
