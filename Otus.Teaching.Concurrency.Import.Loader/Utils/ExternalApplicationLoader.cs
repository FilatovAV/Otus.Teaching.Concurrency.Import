﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.DataObjects;

namespace Otus.Teaching.Concurrency.Import.Loader.Utils
{
    internal class ExternalApplicationLoader
    {
        public bool ProcessResult { get; private set; }

        private readonly string fullPath;
        private readonly int awaitProcMS;
        public ExternalApplicationLoader(LoaderConfig config)
        {
            var procInfo = new ProcessStartInfo()
            {
                Arguments = $"{config.OutputFileName} {config.GenerateDataCount}",
                FileName = config.ExternalApplicationName
            };

            fullPath = Path.Combine(Directory.GetCurrentDirectory(), config.OutputFileName);
            awaitProcMS = config.GenerateDataCount / 100;
            ProcessRun(procInfo);
        }

        private void ProcessRun(ProcessStartInfo processStartInfo)
        {
            var proc = Process.Start(processStartInfo);
            Console.WriteLine($"Process started, please wait {awaitProcMS}ms max.");
            proc.WaitForExit(awaitProcMS);
            ProcessResult = File.Exists(fullPath);
            Console.WriteLine($"The process was completed {(ProcessResult ? "successfully." : "unsuccessfully! File not found.")}");
        }
    }
}
