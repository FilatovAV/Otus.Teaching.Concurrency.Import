﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.Contexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _context;

        public CustomersController(ICustomerRepository context)
        {
            _context = context;
        }

        // GET: api/Customers
        [HttpGet]
        public string GetCustomers()
        {
            return "Welcome to API customers! ;)";
        }

        [HttpGet("CustomerMaxId")]
        public async Task<int> GetCustomerMaxIdAsync()
        {
            return await _context.GetCustomerMaxIdAsync();
        }

        [HttpGet("All")]
        public async Task<IEnumerable<Customer>> GetAllCustomers()
        {
            return await _context.GetAllCustomersAsync();
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(int id)
        {
            var customer = await _context.GetCustomerByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return customer;
        }

        // PUT: api/Customers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomer(int id, Customer customer)
        {
            if (id != customer.Id)
            {
                return BadRequest();
            }
           
            await _context.UpdateCustomerAsync(customer);
            return Accepted();
        }

        // POST: api/Customers
        [HttpPost]
        public async Task<ActionResult<Customer>> PostCustomer(Customer customer)
        {
            try
            {
                await _context.AddCustomerAsync(customer);
            }
            catch (DbUpdateException)
            {
                if (await CustomerExistsAsync(customer.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCustomer", new { id = customer.Id }, customer);
        }

        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customer>> DeleteCustomer(int id)
        {
            var customer = await _context.FindCustomerByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            await _context.DeleteCustomerAsync(customer);

            return customer;
        }

        private async Task<bool> CustomerExistsAsync(int id)
        {
            return await _context.CustomerExistsAsync(id);
        }
    }
}
