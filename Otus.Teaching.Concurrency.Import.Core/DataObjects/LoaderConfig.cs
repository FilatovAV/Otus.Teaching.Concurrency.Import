﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.DataObjects
{
    public class LoaderConfig
    {
        /// <summary> Внешняя или внутренняя генерация данных. </summary>
        public bool ExternalDataGeneration { get; set; }
        /// <summary> Имя внешнего приложения. </summary>
        public string ExternalApplicationName { get; set; }
        /// <summary> Путь к внешнему приложению. По умолчанию Directory.GetCurrentDirectory(). </summary>
        public string ExternalApplicationPath { get; set; }
        /// <summary> Число сгенерированных данных. </summary>
        public int GenerateDataCount { get; set; }
        /// <summary> Имя файла с данными. </summary>
        public string OutputFileName { get; set; }
        /// <summary> Число попыток в случае ошибки. </summary>
        public int NumberOfAttempts { get; set; }
        /// <summary> Тип репозитория. </summary>
        public RepositoryType RepositoryType { get; set; }
    }
}
