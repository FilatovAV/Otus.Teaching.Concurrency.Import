﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.DataObjects
{
    public class LoaderObject
    {
        public int PartNumber { get; set; }
        public int ErrorsCount { get; set; }
        public int AllTaskCount { get; set; }
        public Task Task { get; set; }
        public Exception LastException { get; set; }
        public override string ToString()
        {
            //return string.Join(", ", 
            //    this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
            return string.Join(", ", new string[]
            {
                $"PartNumber: {PartNumber}",
                $"TaskId: {Task.Id}",
                $"ErrorsCount: {ErrorsCount}",
                $"AllThreadCount: {AllTaskCount}"
            });
        }
    }
}