﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.DataObjects
{
    public class ThreadObject
    {
        public int PartNumber { get; set; }
        public int ThreadId { get; set; }
        public int ErrorsCount { get; set; }
        public int AllThreadCount { get; set; }
        public Exception LastException { get; set; }
        public Thread CurThread { get; set; }
        public override string ToString()
        {
            //return string.Join(", ", 
            //    this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
            return string.Join(", ", new string[]
            {
                $"PartNumber: {PartNumber}",
                $"ThreadId: {ThreadId}",
                $"ErrorsCount: {ErrorsCount}",
                $"AllThreadCount: {AllThreadCount}"
            });
        }
    }
}
