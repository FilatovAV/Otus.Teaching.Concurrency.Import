﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.DataObjects
{
    public enum RepositoryType
    {
        None = 0,
        MssqlCustomerRepository = 1,
        SqlLiteCustomerRepository = 2
    }
}
