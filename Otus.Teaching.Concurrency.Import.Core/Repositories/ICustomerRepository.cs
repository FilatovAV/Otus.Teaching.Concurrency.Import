using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddCustomers(Customer[] customers);
        Task AddCustomersAsync(Customer[] customers);
        Task<int> GetCustomerMaxIdAsync();
        Task<IEnumerable<Customer>> GetAllCustomersAsync();
        Task<Customer> GetCustomerByIdAsync(int id);
        Task<Customer> UpdateCustomerAsync(Customer customer);
        Task<bool> CustomerExistsAsync(int id);
        Task AddCustomerAsync(Customer customer);
        Task<Customer> FindCustomerByIdAsync(int id);
        Task DeleteCustomerAsync(Customer customer);
    }
}