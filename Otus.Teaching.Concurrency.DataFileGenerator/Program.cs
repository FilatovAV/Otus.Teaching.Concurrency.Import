﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.DataFileGenerator
{
    class Program
    {
        private static int _dataCount = 1000;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                _dataFilePath = args[0];
                _dataCount = int.TryParse(args[1], out var res) ? res : throw new ArgumentException("Error in argument 2 - number of records!");
            }

            GenerateCustomersDataFile();
        }
        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, _dataCount);
            xmlGenerator.Generate();
        }
    }
}
