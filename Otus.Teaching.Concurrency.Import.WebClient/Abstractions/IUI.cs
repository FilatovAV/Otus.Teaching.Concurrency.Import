﻿namespace Otus.Teaching.Concurrency.Import.WebClient.Abstractions
{
    public interface IUI
    {
        void WriteLine(string message);
        void Write(string message);
        string GetInput(string id);
        void Clear();
    }
}
