﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebClient.Abstractions
{
    public interface IDataProvider
    {
        Task<HttpResponseMessage> GetMessageAsync(string url);
        Task<string> GetAsync(string url);
        Task<string> InitApiAsync();
        Task<string> GetCustomerDataByIdAsync(string id);
        Task<Customer> GetCustomerByIdAsync(string id);
        Task<string> GetAllCustomersDataAsync();
        Task<int> GetCustomerMaxIdAsync();
        Task<HttpResponseMessage> AddNewCustomerAsync(Customer customer);
        Task<Customer> RenameCustomerAsync(Customer customer);
        Task<HttpResponseMessage> DeleteCustomerByIdAsync(string customerId);
    }
}
