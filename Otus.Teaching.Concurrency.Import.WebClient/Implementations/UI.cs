﻿using Otus.Teaching.Concurrency.Import.WebClient.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.WebClient.Implementations
{
    class UI : IUI
    {
        public void Write(string message)
        {
            Console.Write(message);
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public string GetInput(string message)
        {
            Write(message);
            return Console.ReadLine();
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}
