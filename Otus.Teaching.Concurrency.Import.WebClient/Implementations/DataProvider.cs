﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebClient.Abstractions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebClient.Implementations
{
    class DataProvider : IDataProvider
    {
        private readonly HttpClient _httpClient;

        public DataProvider(string baseAddress)
        {
            _httpClient = new HttpClient() { BaseAddress = new Uri(baseAddress) };
        }

        public async Task<string> InitApiAsync()
        {
            var result = await GetAsync(string.Empty);
            return result;
        }

        public async Task<HttpResponseMessage> GetMessageAsync(string url)
        {
            var resp = await _httpClient.GetAsync(url);
            return resp;
        }

        public async Task<string> GetAsync(string url)
        {
            var resp = await _httpClient.GetAsync(url);
            resp.EnsureSuccessStatusCode();
            var result = await resp.Content.ReadAsStringAsync();
            return result;
        }

        public async Task<string> GetAllCustomersDataAsync()
        {
            var res = await GetAsync("all");
            return res;
        }

        public async Task<HttpResponseMessage> AddNewCustomerAsync(Customer customer)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync(string.Empty, content);
            return responseMessage;
        }

        public async Task<Customer> RenameCustomerAsync(Customer customer)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PutAsync($"{customer.Id}", content);
            return responseMessage.StatusCode != System.Net.HttpStatusCode.Accepted ? null : customer;
        }

        public async Task<string> GetCustomerDataByIdAsync(string id)
        {
            var res = await GetAsync($"{id}");
            return res;
        }

        public async Task<int> GetCustomerMaxIdAsync()
        {
            var res = await GetAsync($"CustomerMaxId");
            var customerId = string.IsNullOrEmpty(res) ? -1: int.Parse(res);
            return customerId;
        }

        public async Task<Customer> GetCustomerByIdAsync(string id)
        {
            var res = await GetMessageAsync($"{id}");

            res.EnsureSuccessStatusCode();

            var content = await res.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Customer>(content);
            return result;
        }

        public async Task<HttpResponseMessage> DeleteCustomerByIdAsync(string customerId)
        {
            var responseMessage = await _httpClient.DeleteAsync($"{customerId}");
            return responseMessage;
        }
    }
}
