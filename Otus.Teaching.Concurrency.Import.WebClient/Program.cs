﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.WebClient.Implementations;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.WebClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory
                .GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true);
            var config = configBuilder.Build();

            var webApiBaseAddress = config["WebApiBaseAddress"];

            var uInterface = new UI();
            var dProvider = new DataProvider(webApiBaseAddress);
            var dc = new DataClient(uInterface, dProvider);
            dc.GetCommands();

            Console.ReadLine();
        }
    }
}
