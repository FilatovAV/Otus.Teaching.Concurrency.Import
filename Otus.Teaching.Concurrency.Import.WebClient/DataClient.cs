﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebClient.Abstractions;
using Otus.Teaching.Concurrency.Import.WebClient.Implementations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otus.Teaching.Concurrency.Import.WebClient
{
    public class DataClient
    {
        private readonly IUI _ui;
        private readonly IDataProvider _dataProvider;

        public DataClient(IUI userInterface, IDataProvider dataProvider)
        {
            _ui = userInterface;
            _dataProvider = dataProvider;
            ConnectAsync().GetAwaiter().GetResult();
        }

        private async Task ConnectAsync()
        {
            try
            {
                _ui.WriteLine("Start the connection to API Customers...");
                var result = await _dataProvider.InitApiAsync();
                _ui.WriteLine($"Response: {result}");
                _ui.WriteLine($"Connection was successful.");
            }
            catch (Exception ex)
            {
                _ui.WriteLine(ex.ToString());
            }
        }

        public void GetCommands()
        {
            _ui.WriteLine("\nAPI commands:");
            _ui.WriteLine("1 - get customer by id;");
            _ui.WriteLine("2 - get all customers (a long-running operation);");
            _ui.WriteLine("3 - create a fake customer;");
            _ui.WriteLine("4 - rename customer by id;");
            _ui.WriteLine("5 - get customer max id;");
            _ui.WriteLine("6 - delete customer by id;");
            _ui.WriteLine("0 - exit.");

            var result = _ui.GetInput("Input command:");
            CommandExecutor(result).GetAwaiter().GetResult();
        }

        public async Task<Customer> GetCustomerByIdAsync()
        {
            var id = _ui.GetInput("Input customer id:");

            Customer customer;
            try
            {
                customer = await _dataProvider.GetCustomerByIdAsync(id);
            }
            catch (HttpRequestException ex)
            {
                _ui.WriteLine(ex.Message);
                return null;
            }

            _ui.WriteLine("Customer found:");

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(customer);
            _ui.WriteLine(json);

            return customer;
        }

        public async Task GetCustomerDataByIdAsync()
        {
            var res = string.Empty;
            var id = _ui.GetInput("Input customer id:");

            try
            {
                res = await _dataProvider.GetCustomerDataByIdAsync(id);
            }
            catch (HttpRequestException ex)
            {
                _ui.WriteLine(ex.Message);
            }

            _ui.WriteLine(res);
        }

        public async Task GetAllCustomersDataAsync()
        {
            _ui.WriteLine("Please wait...");
            var res = await _dataProvider.GetAllCustomersDataAsync();
            _ui.WriteLine(res);
        }

        private async Task CommandExecutor(string command)
        {
            _ui.Clear();
            switch (command)
            {
                case "1":
                    await GetCustomerDataByIdAsync();
                    break;
                case "2":
                    await GetAllCustomersDataAsync();
                    break;
                case "3":
                    await AddFakeCustomerAsync();
                    break;
                case "4":
                    await RenameCustomerByIdAsync();
                    break;
                case "5":
                    await GetCustomerMaxIdAsync();
                    break;
                case "6":
                    await DeleteCustomerByIdAsync();
                    break;
                case "0":
                    _ui.Write("Goodbye!");
                    return;
                default:
                    _ui.Write("\nWrong command!\n");
                    break;
            }
            GetCommands();
        }

        private async Task DeleteCustomerByIdAsync()
        {
            var id = _ui.GetInput("Enter the customer id to delete: ");
            var res = await _dataProvider.DeleteCustomerByIdAsync(id);

            if (res.IsSuccessStatusCode)
            {
                _ui.WriteLine($"User id {id} has been deleted.");
                return;
            }

            _ui.WriteLine($"An error occurred when deleting the customer. Error code: {(int)res.StatusCode}.");
        }

        private async Task RenameCustomerByIdAsync()
        {
            var customer = await GetCustomerByIdAsync();
            if (customer is null) { return; }
            var name = _ui.GetInput("Enter the customer full name: ");
            if (string.IsNullOrEmpty(name))
            {
                _ui.WriteLine("Invalid customer name!");
                return;
            }
            customer.FullName = name;
            var renamedCustomer =  await _dataProvider.RenameCustomerAsync(customer);
            if (renamedCustomer is null) { return; }
            _ui.WriteLine("Customer information has been updated.");
        }

        private async Task GetCustomerMaxIdAsync()
        {
            var id = await _dataProvider.GetCustomerMaxIdAsync();
            _ui.WriteLine($"The maximum customer ID is {id}.");
        }

        private async Task AddFakeCustomerAsync()
        {
            var customer = RandomCustomerGenerator.Generate(1).FirstOrDefault();

            var customerId = await _dataProvider.GetCustomerMaxIdAsync();
            customer.Id = ++customerId;

            var responseMessage = await _dataProvider.AddNewCustomerAsync(customer);

            if (responseMessage.StatusCode != System.Net.HttpStatusCode.Created)
            {
                _ui.WriteLine($"An error occurred while adding the customer. Error code : {(int)responseMessage.StatusCode}.");
                return;
            }

            var result = responseMessage.Content.ReadAsStringAsync().Result;

            _ui.WriteLine(result);
        }
    }
}
