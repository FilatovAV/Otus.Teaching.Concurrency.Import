using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public XmlGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }
        
        public void Generate()
        {
            Console.WriteLine($"Creating an XML data...");
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
                Console.WriteLine($"The old file was deleted.");
            }
            using var stream = File.Create(_fileName);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
            Console.WriteLine($"File creation is complete and the total number of entries is {_dataCount}.");
        }
    }
}