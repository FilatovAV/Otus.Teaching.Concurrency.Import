﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }

        public Task<List<Customer>> ParseAsync()
        {
            var task = new Task<List<Customer>>(Parse);
            task.Start();
            return task;
        }

        public List<Customer> Parse()
        {
            Console.WriteLine("Parse data...");
            var serializer = new XmlSerializer(typeof(CustomersList));
            using var stream = File.OpenRead(_fileName);
            var result = (CustomersList)serializer.Deserialize(stream);
            Console.WriteLine("Data parsing is finished.");

            return result.Customers;
        }

    }
}