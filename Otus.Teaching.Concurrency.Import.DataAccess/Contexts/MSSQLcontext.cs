﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Contexts
{
    public class MsSqlContext : DbContext
    {
        private static string cs = $@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ConcurrencyImport;Integrated Security=True";

        public DbSet<Customer> Customers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(cs);
    }
}
