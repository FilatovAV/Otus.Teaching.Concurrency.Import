﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Contexts
{
    public class SQLiteContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($@"Data Source={Directory.GetCurrentDirectory()}\ConcurrencyImport.db");
    }
}
