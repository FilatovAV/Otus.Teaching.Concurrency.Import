﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.DataObjects;
using Otus.Teaching.Concurrency.Import.DataAccess.Contexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using SQLitePCL;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class MssqlCustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly MsSqlContext _db;

        public MssqlCustomerRepository()
        {
            _db = new MsSqlContext();
        }

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(customer);
            _db.SaveChanges();
        }

        public void AddCustomers(Customer[] customers)
        {
            _db.Customers.AddRange(customers);
            var i = _db.SaveChanges();
            SaveComplitedMessage(i);
        }
        public async Task AddCustomersAsync(Customer[] customers)
        {
            await _db.Customers.AddRangeAsync(customers);
            var i = await _db.SaveChangesAsync();
            SaveComplitedMessage(i);
        }
        private void SaveComplitedMessage(int i)
        {
            Console.WriteLine($"The data range was saved, total number of entries is {i}.");
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        public Task<int> GetCustomerMaxIdAsync()
        {
            return _db.Customers.MaxAsync(m => m.Id);
        }

        public async Task<IEnumerable<Customer>> GetAllCustomersAsync()
        {
            return await _db.Customers.ToListAsync<Customer>();
        }

        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await _db.Customers.FindAsync(id);
        }

        public async Task<Customer> UpdateCustomerAsync(Customer customer)
        {
            _db.Entry(customer).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return customer;
        }

        public async Task<bool> CustomerExistsAsync(int id)
        {
            return await _db.Customers.AnyAsync(a => a.Id == id);
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _db.Customers.AddAsync(customer);
            await _db.SaveChangesAsync();
        }

        public async Task<Customer> FindCustomerByIdAsync(int id)
        {
            return await _db.Customers.FindAsync(id);
        }

        public async Task DeleteCustomerAsync(Customer customer)
        {
            _db.Customers.Remove(customer);
            await _db.SaveChangesAsync();
        }
    }
}
