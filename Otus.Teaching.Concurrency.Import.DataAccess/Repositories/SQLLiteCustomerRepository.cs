using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess.Contexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class SqlLiteCustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly SQLiteContext _db;

        public SqlLiteCustomerRepository()
        {
            _db = new SQLiteContext();
        }

        public void AddCustomer(Customer customer)
        {
            _db.Customers.Add(customer);
            _db.SaveChanges();
        }

        public void AddCustomers(Customer[] customers)
        {
            _db.Customers.AddRange(customers);
            var i = _db.SaveChanges();
            SaveComplitedMessage(i);
        }

        public async Task AddCustomersAsync(Customer[] customers)
        {
            await _db.Customers.AddRangeAsync(customers);
            var i = await _db.SaveChangesAsync();
            SaveComplitedMessage(i);
        }

        private void SaveComplitedMessage(int i)
        {
            Console.WriteLine($"The data range was saved, total number of entries is {i}.");
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        public Task<int> GetCustomerMaxIdAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Customer>> GetAllCustomersAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Customer> GetCustomerByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Customer> UpdateCustomerAsync(Customer customer)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CustomerExistsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task AddCustomerAsync(Customer customer)
        {
            throw new NotImplementedException();
        }

        public Task<Customer> FindCustomerByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task DeleteCustomerAsync(Customer customer)
        {
            throw new NotImplementedException();
        }
    }
}